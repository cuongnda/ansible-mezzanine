from local import *

# # Make these unique, and don't share it with anybody.
# SECRET_KEY = "4cz=aiqmd1julg75$3m^px)jn4*w4&n23bgixpk&3t7(w3d7_&"
# NEVERCACHE_KEY = "r%)ifm3)#nw3scobmr&*^^@&tt3)w*fu712=eo+&he#ratebe1"
#
# ALLOWED_HOSTS = ["128.199.102.178"]
#
# DATABASES = {
#     "default": {
#         # Ends with "postgresql_psycopg2", "mysql", "sqlite3" or "oracle".
#         "ENGINE": "django.db.backends.postgresql_psycopg2",
#         # DB name or path to database file if using sqlite3.
#         "NAME": "mezzanine_ansible",
#         # Not used with sqlite3.
#         "USER": "mezzanine_ansible",
#         # Not used with sqlite3.
#         "PASSWORD": "password",
#         # Set to empty string for localhost. Not used with sqlite3.
#         "HOST": "",
#         # Set to empty string for default. Not used with sqlite3.
#         "PORT": "",
#     }
# }
