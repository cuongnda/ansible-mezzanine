from django.shortcuts import render

# Create your views here.
from myapp.forms import LegalDocumentForm
from myapp.models import LegalDocument


def legal_document_form(request, template="pages/legal_document_form.html"):
    """
    This view allow user to search for order document
    """
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        # form = LegalDocumentForm(request.POST)
        # check whether it's valid:

        result = []

        # code = form.cleaned_data['code']
        # document_type = form.cleaned_data['document_type']
        #Search for document
        code = request.POST['new_field']


        docs = LegalDocument.objects.filter(code=code)

        for doc in docs:
            result.append(doc)
        return render(request, template,
               {'results': result})

    # if a GET (or any other method) we'll create a blank form
    else:
        # form = LegalDocumentForm()

        return render(request, template,
                      {})
