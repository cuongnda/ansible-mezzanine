from __future__ import unicode_literals

from django.db import models

# Create your models here.
from mezzanine.pages.models import Page


class LegalDocumentField(Page):
    name = models.CharField(max_length=255, default="default")
    desch = models.CharField(max_length=255, default="default")

class LegalDocument(Page):
    code = models.CharField(max_length=255, default="default")
    issue = models.CharField(max_length=255, default="default")

    desc = models.CharField(max_length=255, default="default")
