# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms
from django.utils.translation import ugettext_lazy as _


class LegalDocumentForm(forms.Form):
    code = forms.CharField(label=_(u"So hieu"),
                           required=False)

    document_type = forms.ChoiceField(label=_("Linh vuc van ban"),
                                            choices=[('', 'Linh vuc van ban'), ('1', '1 '), ('2', '2 '),
                                                     ('3', '3 '), ('4', '4 ')],
                                            required=False)
