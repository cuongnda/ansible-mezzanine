import datetime
import random
import urllib2
from mezzanine import template
from mezzanine.blog.models import BlogPost, BlogCategory

register = template.Library()


@register.simple_tag
def get_all_blog_by_category(category):
    import ipdb;
    ipdb.set_trace()
    #blogs = BlogPost.objects.filter(category=category)
    blog_category = BlogCategory.objects.filter(category=category)

    return blog_category


@register.simple_tag
def current_time(format_string):
    return "TEst"

@register.inclusion_tag('blog_list.html')
def get_all_blog_by_category(category_slug):

    blogs = []

    categories = BlogCategory.objects.filter(slug=category_slug)
    for category in categories:
        blogposts = category.blogposts
        for blog in blogposts.all():
            blogs.append(blog)
    return {'blogs': blogs}