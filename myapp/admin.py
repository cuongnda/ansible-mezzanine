from django.contrib import admin

# Register your models here.
from mezzanine.pages.admin import PageAdmin

from myapp.models import LegalDocument


class LegalDocumentAdmin(PageAdmin):
    pass

admin.site.register(LegalDocument, LegalDocumentAdmin)
